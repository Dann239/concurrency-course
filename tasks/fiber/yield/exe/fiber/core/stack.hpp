#pragma once

#if defined(__TWIST_ISOLATED_SIM__)

namespace exe::fiber {

#include <sure/stack/new.hpp>

// Adapt to deterministic simulation
using Stack = ::sure::NewStack;

}  // namespace exe::fiber

#else

#include <sure/stack/mmap.hpp>

namespace exe::fiber {

using Stack = ::sure::MmapStack;

}  // namespace exe::fiber

#endif
