#include <exe/thread/wait_group.hpp>

#include <course/test/twist.hpp>

#include <twist/ed/std/thread.hpp>

#include <twist/build.hpp>

static_assert(twist::build::IsolatedSim());

using exe::thread::WaitGroup;

TEST_SUITE(WaitGroup) {

  /*
   * Equivalent to:
   *
   * ThreadPool pool{4};
   * pool.Start();
   *
   * {
   *   WaitGroup wg;
   *
   *   wg.Add(1);
   *   pool.Submit([&wg] {
   *     wg.Done();
   *   });
   *
   *   wg.Wait();
   * }
   *
   * pool.Stop();
   */

  TWIST_RANDOMIZE(Storage, 5s) {
    WaitGroup* wg = new WaitGroup{};

    wg->Add(1);
    twist::ed::std::thread t([&wg] {
      wg->Done();
    });

    wg->Wait();
    delete wg;

    t.join();
  }
}

RUN_ALL_TESTS()
