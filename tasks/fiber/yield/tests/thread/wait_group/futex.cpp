#include <exe/thread/wait_group.hpp>

#include <wheels/test/framework.hpp>

#include <twist/sim.hpp>
#include <twist/build.hpp>

static_assert(twist::build::IsolatedSim());

using exe::thread::WaitGroup;

TEST_SUITE(WaitGroup) {
  SIMPLE_TEST(Futex) {
    twist::sim::sched::FairScheduler scheduler;
    twist::sim::Simulator simulator{&scheduler};

    auto result = simulator.Run([] {
      WaitGroup wg;

      static const size_t kIters = 3;
      static const size_t kWorkPerIter = 17;

      for (size_t k = 0; k < kIters; ++k) {
        // 0

        for (size_t i = 0; i < kWorkPerIter; ++i) {
          wg.Add(1);
        }

        for (size_t i = 0; i < kWorkPerIter; ++i) {
          wg.Done();
        }

        wg.Wait();
      }
    });

    ASSERT_TRUE(result.Ok());
    ASSERT_TRUE(simulator.FutexCount() == 0);
  }
}

RUN_ALL_TESTS()
