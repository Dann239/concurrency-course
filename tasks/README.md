# Задачи

- [`sync`](sync) – блокирующая синхронизация для потоков
- [`sched`](sched) – планировщики
- [`future`](future) – concurrency / futures
- [`fiber`](fiber) – concurrency / stackful fibers